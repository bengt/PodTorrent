# /usr/bin/env python

#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

from optparse import OptionParser
from os import system, path, stat
from time import *
from hashlib import md5, sha1
from urlparse import urlparse
import httplib
import pickle
from pprint import pprint

from types import IntType, LongType, StringType, ListType, TupleType, DictType
import re
from cStringIO import StringIO

import feedparser


class MetaFile (object):

    mtime = None
    name = None
    size = None
    type = None


    def __init__(self, name, type):
        self.name = name
        self.type = type

class Ressource (MetaFile):

    url = None

    def __init__(self, type, url):
        MetaFile.__init__(self, urlparse(url).path[1:], type)
        self.url = url

    # determine whether local file is current and act accordingly
    def update (self):
        if self.isCurrent():
            print 'DEBUG: Local copy of', self.type ,'was current:', self.url
            return True
        else:
            print 'INFO: Updating', self.type, ':', self.url
            self.upgrade()
            return False

    # download the remote ressource
    def upgrade(self):
        if self.type == 'feed':
            system('wget -P ./' + self.type + 's/ -N ' + self.url)
        if self.type == 'episode':
            system('wget -c -P ./' + self.type + 's/ -N ' + self.url)

    # return whether a remote resource is the same as its local copy
    def isCurrent (self):

        filename, mdate_r, size_r = self.getRessourceInfo('remote', self.type, self.url)
        filename, mdate_l, size_l = self.getRessourceInfo('local', self.type, self.url)

        # compare attributes of the files
        sameTimestamp = mdate_l == mdate_r
        sameFilesize = int(size_r) == int(size_l)
        current = sameTimestamp and sameFilesize

        return current

    # determine local modification timestamp string and filesize
    def getRessourceInfo (self, locality, type, url):
        # parse url and extract the filename from the remote ressource
        ressource = urlparse(url)
        filename = path.split(ressource.path)[1]
        if type == 'episode':
            dir = './episodes/'
        elif type == 'feed':
            dir = './feeds/'
        else:
            print "ERROR: Unknown type in getRessourceInfo()"
            return

        # make GET-request, parse the headers
        if locality == 'remote':
            conn = httplib.HTTPConnection(ressource.netloc)
            conn.request('GET', ressource.path)
            r = conn.getresponse()
            for key, value in r.getheaders():
                if key == 'last-modified': mtime = value
                elif key == 'content-length': size = value
            conn.close()
        # get fileattributes, convert to timestring
        elif locality == 'local':
            try:
                mtime = int(path.getmtime(dir + filename))
            except OSError:
                mtime = 0
            mtime = strftime("%a, %d %b %Y %H:%M:%S GMT", gmtime(mtime))
            try:
                size = stat(dir + filename).st_size
            except OSError:
                size = 0
        else:
            print "ERROR: Unknown locality in getRessourceInfo()"
            return
        #print "ResourceInfo:", url, type, locality, filename, mtime, size
        return filename, mtime, size

class Torrent (MetaFile):

    type = 'torrent'
    tracker = None
    pieceSize = None
    private = None
    description = None

    infoHash = None

    def __init__(size, mtime, name):
        MetaFile.__init__(self, name, 'torrent')
        self.tracker = tracker
        self.pieceSize = pieceSize
        self.private = private

    # make sure that torrent file is current
    def updateTorrent (self):
        epName = epName[:-len('.mp3')]

        if not epName == None:
            print 'DEBUG: Was already created:', epName + '.torrent'
            return

        print 'INFO: Creating metafile:', epName + '.torrent'
        infoHash = self.generateTorrent(epName, epDescription, epDate)

        self.torrents.append([epName, infoHash])
        self.saveState()

    def bencode_rec(self, x, b):
        t = type(x)
        if t in (IntType, LongType):
            b.write('i%de' % x)
        elif t is StringType:
            b.write('%d:%s' % (len(x), x))
        elif t in (ListType, TupleType):
            b.write('l')
            for e in x:
                bencode_rec(e, b)
            b.write('e')
        elif t is DictType:
            b.write('d')
            keylist = x.keys()
            keylist.sort()
            for k in keylist:
                assert type(k) is StringType
                self.bencode_rec(k, b)
                self.bencode_rec(x[k], b)
            b.write('e')
        else:
            assert 0

    def bencode(self, x):
        b = StringIO()
        self.bencode_rec(x, b)
        return b.getvalue()

    def makeinfo(self, filename, piece_length, encoding, flag):

        size = stat('./episodes/' + filename).st_size

        pieces = []
        p = 0L
        # calculate sha for chunks
        f = open('./episodes/' + filename, 'rb')
        while p < size:
            x = f.read(min(2 ** piece_length, size - p))
            pieces.append(sha1(x).digest())
            p += 2 ** piece_length
            if p > size: p = size
        f.close()

        # calculate md5 hash for file
        f = open('./episodes/' + filename, 'r')
        md5sum = md5()
        while True:
            bite = f.read(2 ** piece_length) # feed md5sum one piece at a time
            if not bite: break
            md5sum.update(bite)
        md5sum = md5sum.hexdigest()
        f.close()

        return {
            'pieces': ''.join(pieces),
            'piece length': 2 ** piece_length,
            'length': size,
            'name': filename,
            'md5sum': md5sum
        }

    def generate (self, epName, epDescription, epDate):

        print epName, epDescription, epDate

        info = self.makeinfo(
            epName.decode('utf-8').encode('ascii') + '.mp3',
            int(self.pieceSize), 'utf-8', self.private
        )

        metadata = {
            'info': info,
            'announce': self.tracker,
            'creation date': long(time()),
            'comment': epName.decode('utf-8').encode('ascii'),
        }
        metadata = self.bencode(metadata)

        h = open('./torrents/' + epName + '.torrent', 'wb')
        h.write(metadata)
        h.close()

        info = self.bencode(info)
        s = sha1()
        s.update(info)
        self.infoHash = s.hexdigest()

class Episode (Ressource):

    type = 'episode'

    description = None
    title = None

    torrent = None # corresponding torrent

    def __init__(self, url, description, title):
        Ressource.__init__(self, 'episode', url)
        self.createTorrent()

    def createTorrent(self):
        self.torrent = Torrent(self.description, self.title, self.name)

    # make sure that there is a describing torrent
    def upgradeEpisode (self):
        if not self.update(): self.upgradeEpisode()

    def updateEpisode (self):


class Feed (Ressource):

    episodes = [] # list of corresponding episodes
    parsed = None
    details = [None, None, None]

    def __init__ (self, url):
        Ressource.__init__(self, 'feed', url)
        self.updateFeed()

    # collect relevant data from a single episode of the feed
    def harvestEntry (self, entry):
        for enclosure in entry.enclosures:
            if enclosure['href']:
                url = enclosure['href']
        for option in entry.content:
            if option.type == 'text/plain':
                description = option.value
        title = entry.title
        return description, title, url

    # make sure that all episodes of one feed have a current local copy
    def updateFeed (self):
        if self.update(): self.parseFeed()
    def parseFeed(self):
        parsed = self.parsed
        self.parsed = feedparser.parse('./feeds/%s' % self.name)
        if parsed != self.parsed and parsed != None:
            self.reInit()
    def reInit (self):
        self.episodes = []
        for entry in self.parsed.entries:
            description, title, url = self.harvestEntry(entry)
            self.episodes.append(Episode(url, description))
            print "INFO: Found new episode" + description, title, url, "."
        self.details = self.getPodCastDetails()

    # make sure that all episodes are current
    def upgradeFeed (self):
        self.updateEpisodes()
    def updateEpisodes (self):
        for episode in self.episodes:
            episode.update()

    # return details of feed
    def getPodCastDetails (self):
        name, description, homepage = self.details
        self.details[0] = self.parsed.channel.title
        self.details[1] = self.parsed.channel.description
        self.details[2] = self.parsed.channel.link
        return self.details

class PodCast (object):

    # metainfo
    name = None
    description = None
    homepage = None
    # configuration
    tracker = None
    pieceSize = None
    private = None
    # payload
    feeds = []

    def __init__(self, tracker, pieceSize, private, feedURLs):
        self.tracker = tracker
        self.pieceSize = pieceSize
        self.private = private
        for feedURL in feedURLs:
            self.feeds.append(Feed(feedURL))

    # make sure that all feeds are current
    def updatePodCast (self):
        for feed in self.feeds:
            feed.updateFeed()
        self.upgradePodCast()

    # collect information about this podcast from its feeds
    def upgradePodCast (self):
        for feed in self.feeds:
            name, description, homepage = feed.getPodCastDetails()
        print name, description, homepage

class PodTorrent:

    podCasts = []    # list of podcasts, see config

    def __init__(self, tracker, pieceSize, private, feedURLs):
        # TODO: parse URL to determine the podcast it belongs to
        podCasts = []
        for feedURL in feedURLs:
            if feedURL[len(feedURL) - len('-complete.rss'):] == '-complete.rss':
                podCasts.append(feedURL[len('http://'): - len('-complete.rss')])
            if feedURL[len(feedURL) - len('-latest.rss'):] == '-latest.rss':
                podCasts.append(feedURL[len('http://'): - len('-latest.rss')])
        for podCast in podCasts:
            feeds = []
            if 'http://' + podCast + '-latest.rss' in feedURLs:
                feeds.append('http://' + podCast + '-latest.rss')
            if 'http://' + podCast + '-complete.rss' in feedURLs:
                feeds.append('http://' + podCast + '-complete.rss')
            podCast = PodCast(tracker, pieceSize, private, feeds)
            self.podCasts.append(podCast)

        # set self.PodCasts
        #self.loadState()

    def run(self):
        for podCast in self.podCasts:
            podCast.updatePodCast()

        # register Torrents with Tracker
        #self.updateWhitelist()

    # unpickle state from statefile
    def loadState (self):
        try:
            stateFile = open('./state.pkl', 'rb')
            print 'DEBUG: Loading state file.'
            try:
                self.torrents = pickle.load(stateFile)
            except EOFError:
                pass # empty file, nothing to worry about
        except IOError:
            print 'INFO: First run. Creating state file.'
            stateFile = open('./state.pkl', 'w')
        stateFile.close()

    # pickle state into statefile
    def saveState (self):
        stateFile = open('./state.pkl', 'w')
        pickle.dump(self.torrents, stateFile)
        stateFile.close()

    # make sure that all torrents have an entry at the whitelist
    def updateWhitelist (self):
        w = open('whitelist.list', 'w')
        for infoHash in self.torrents: w.write(str(infoHash[1]) + '\n')
        w.close()

# handle commandline options / configuration
def main ():

    version = '0.4'

    # parse torrents.conf
    def parseTorrentsConf ():
        tracker = None
        pieceSize = 19
        private = 0
        feeds = []
        for line in open('./PodTorrent.conf'):
            if (line[:len('#')] == '#' or line[:len('\n')] == '\n'):
                pass
            elif (line[:len('tracker')] == 'tracker'):
                tracker = line[len('tracker') + 3:-1]
            elif (line[:len('pieceSize')] == 'pieceSize'):
                pieceSize = int(line[len('pieceSize') + 3:-1])
            elif (line[:len('private')] == 'private'):
                private = line[len('private') + 3:-1]
            elif (line[:len('http://')] == 'http://'):
                feeds.append(line[:-1])
            else:
                print 'WARN: Unrecognised line in config:', line
        return tracker, pieceSize, private, feeds

    # parse commandline options
    def parseCommandlineOptions (tracker, pieceSize, private, feed, version):
        parser = OptionParser(usage='%prog [-f] [-q]', \
                              version='%prog.' + version)
        parser.add_option('-t', '--tracker', dest='tracker',
                          help='use TRACKER (announce url)', metavar='TRACKER')
        parser.add_option('-s', '--pieceSize', dest='pieceSize',
                          help='use PIECESIZE (power of 2) Default is 19, 512 \
                          kiB per chunk.', metavar='PIECESIZE')
        parser.add_option('-p', '--private', dest='private',
                          help='set torrents PRIVATE (0|1)', metavar='PRIVATE')
        parser.add_option('-f', '--feed', dest='feed',
                          help='track FEED (url)', metavar='FEED')
        (options, args) = parser.parse_args()

        # Merge options with config
        if options.tracker: tracker = options.tracker
        if options.pieceSize: pieceSize = options.pieceSize
        if options.private: private = options.private
        if options.feed:
            feed = []
            feed.append(options.feed)

        return tracker, pieceSize, private, feed

    # merge configs
    tracker, pieceSize, private, feeds = parseTorrentsConf()
    tracker, pieceSize, private, feeds = parseCommandlineOptions(
            tracker, pieceSize, private, feeds, version)

    # handle configuration / invocation error
    if tracker == None:
        print 'ERROR: No valid tracker found. Please specify an announce URL!'
        return

    PodTorrent(tracker, pieceSize, private, feeds).run()

# hook to make this thing run by default
if __name__ == '__main__':
    main()
